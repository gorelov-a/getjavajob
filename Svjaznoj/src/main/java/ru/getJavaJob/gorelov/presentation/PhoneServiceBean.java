package ru.getJavaJob.gorelov.presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.getJavaJob.gorelov.dao.Page;
import ru.getJavaJob.gorelov.dao.PhoneDao;
import ru.getJavaJob.gorelov.model.Phone;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Антон on 22.02.2015.
 */
@Service
public class PhoneServiceBean implements PhoneService {
    @Autowired
    private PhoneDao phoneDao;

    @Override
    @Transactional
    public List<Phone> getAllPhones() {
        return phoneDao.getAllPhones();
    }

    @Override
    @Transactional
    public Page<Phone> getPage(int page, int size) {
        return phoneDao.getPage(page, size);
    }

    @Override
    @Transactional
    public Page<Phone> savePhones(List<Phone> phones) {
        Iterator<Phone> iterator = phones.iterator();

        while (iterator.hasNext()) {
            Phone elem = iterator.next();
            if (!elem.getPhone().matches("7\\d{10}")) {
                iterator.remove();
            }
        }

        phoneDao.addPhone(phones);

        return phoneDao.getPage(1, 10);
    }

    public void setPhoneDao(PhoneDao phoneDao) {
        this.phoneDao = phoneDao;
    }
}