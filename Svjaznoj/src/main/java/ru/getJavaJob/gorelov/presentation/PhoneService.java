package ru.getJavaJob.gorelov.presentation;

import ru.getJavaJob.gorelov.dao.Page;
import ru.getJavaJob.gorelov.model.Phone;

import java.util.List;

/**
 * Created by Антон on 22.02.2015.
 */
public interface PhoneService {
    List<Phone> getAllPhones();

    Page<Phone> getPage(int page, int size);

    Page<Phone> savePhones(List<Phone> phones);
}
