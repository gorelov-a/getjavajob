package ru.getJavaJob.gorelov.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.getJavaJob.gorelov.dao.Page;
import ru.getJavaJob.gorelov.model.Phone;
import ru.getJavaJob.gorelov.presentation.PhoneService;

import javax.xml.ws.Response;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Антон on 22.02.2015.
 */
@Controller
public class PhoneController {
    private final PhoneService phoneService;

    @Autowired
    public PhoneController(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    @RequestMapping("/")
    public String getAllPhones(@RequestParam(defaultValue = "1", value = "page", required = false) int page,
                               @RequestParam(defaultValue = "10", value = "size", required = false) int size, Model model) {
        Page phonePage = phoneService.getPage(page, size);
        model.addAttribute("page", phonePage);
        return "index";
    }

    @RequestMapping("/getPage")
    public String getPage(@RequestParam(defaultValue = "1", value = "page", required = false) int page,
                          @RequestParam(defaultValue = "10", value = "size", required = false) int size, Model model) {
        Page phonePage = phoneService.getPage(page, size);
        model.addAttribute("page", phonePage);
        return "page";
    }

    @RequestMapping("/savePhones")
    public void savePhones(@RequestBody String phones) {
        ObjectMapper mapper = new ObjectMapper();
        List<Phone> myObjects = null;
        try {
            myObjects = mapper.readValue(phones, mapper.getTypeFactory().constructCollectionType(List.class, Phone.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        phoneService.savePhones(myObjects);
    }
}
