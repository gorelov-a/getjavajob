package ru.getJavaJob.gorelov.dao;

import ru.getJavaJob.gorelov.model.Phone;

import java.util.List;

/**
 * Created by Антон on 21.02.2015.
 */
public interface PhoneDao {
    void addPhone(List<Phone> phone);

    Page<Phone> getPage(int currPage, int size);

    List getAllPhones();
}
