package ru.getJavaJob.gorelov.dao;

import ru.getJavaJob.gorelov.model.Phone;
import ru.getJavaJob.gorelov.dao.Page;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Антон on 22.02.2015.
 */
public class PhoneDaoImpl implements PhoneDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void addPhone(List<Phone> phones) {
        for (Phone phone : phones) {
            em.persist(phone);
        }
    }

    @Override
    public Page<Phone> getPage(int currPage, int size) {
        long count = em.createNamedQuery("getCountPhones", Long.class).getSingleResult();
        List<Phone> phones = em.createNamedQuery("getAllPhones",Phone.class).setFirstResult((currPage-1)*size).setMaxResults(size).getResultList();
        return new Page<>(count, phones, 10);
    }

    @Override
    public List getAllPhones() {
        return em.createNamedQuery("getAllPhones").getResultList();
    }
}
