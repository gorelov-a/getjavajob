package ru.getJavaJob.gorelov.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Антон on 21.02.2015.
 */
@NamedQueries({
        @NamedQuery(name = "getCountPhones", query = "select count (p) from Phone p"),
        @NamedQuery(name = "getAllPhones", query = "select p from Phone p")
})

@Entity
@Table(name = "PHONE_BOOK")
public class Phone {
    @Id
    @Column(name = "PHONE_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;

    @Column(name = "PHONE")
    @JsonProperty("Number")
    private String phone;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE")
    @JsonIgnore
    private Date date;

    public Phone() {
        id = -1;
        phone = "";
        date = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone1 = (Phone) o;

        if (id != phone1.id) return false;
        if (phone != phone1.phone) return false;
        if (!date.equals(phone1.date)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + phone.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Phone[Number: " + phone + "]";
    }
}
