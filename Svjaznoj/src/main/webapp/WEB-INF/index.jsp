<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Телефонная книга</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
        function validate() {
            var value = $("#newPhones").val();
            var noValid = "";
            var valid = [];
            var reqEx = /\s*\n\s*/;
            var phones = value.split(reqEx);

            for (var i = 0; i < phones.length; ++i) {
                if (phones[i].match(/7\d{10}/)) {
                    phone = {};
                    phone.Number = phones[i];
                    valid.push(phone);
                } else {
                    noValid += phones[i] + "\n";
                }
            }

            $("#incorrectPhones").val(noValid);
            return valid;
        }

        function savePhone() {
            var valid = validate();
            valid = JSON.stringify(valid);

            $.ajax({
                type: "POST",
                url: "/savePhones",
                contentType: "application/json",
                dataType: 'json',
                data: valid
            });
        }

    </script>
</head>
<body>
<table align="center" width="95%">
    <tr>
        <th width="33%">Новые номера</th>
        <th width="33%">Ошибки в номерах</th>
        <th width="33%">Телефонная книга</th>
    </tr>
    <tr>
        <td width="33%"></td>
        <td width="33%"></td>
        <td align="center" width="33%">
            <table>
                <tr>
                    <th width="50%">
                        Телефон
                    </th>
                    <th width="50%">
                        Дата обработки
                    </th>
                </tr>
            </table>
        </td>
    <tr>
    <tr>
        <td align="center" valign="top" width="33%"><textarea id="newPhones" name="comment" cols="45" rows="40"
                                                              autofocus required title="Новые номера"></textarea></td>
        <td align="center" valign="top" width="33%"><textarea id="incorrectPhones" name="comment" cols="45" rows="40"
                                                              readonly title="Номер с ошибками"></textarea></td>
        <td align="center" valign="top" width="33%">
            <jsp:include page="/getPage"></jsp:include>
        </td>
    </tr>
    <tr>
        <td align="right"><input onclick="savePhone()" type="button" value="Сохранить"></td>
        <td colspan="2"></td>
    </tr>
</table>
</body>
</html>
