<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<textarea name="comment" cols="45" rows="40" readonly>
    <c:forEach var="record" items="${page.records}">
        <c:out value="${record.getPhone()}                     ${record.getDate()}"/>
    </c:forEach>
</textarea>
<ul class="pagination">
    <c:forEach var="i" begin="1" end="${page.totalPages}">
        <li><a href="?page=${i}" class="page">${i}</a></li>
    </c:forEach>
</ul>
